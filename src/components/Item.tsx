import React from 'react';

export const Item = ({ item: { text }}) => (
  <li>{text}</li>
);
