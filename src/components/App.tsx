import React from 'react';

import { Item } from './Item';

const list = [
  { id: 0, text: 'Item 1' },
  { id: 1, text: 'Item 2' },
  { id: 2, text: 'Item 3' }
];

export const App = () => (
  <div>
    <p>Here's a list</p>
    <ul>
      {list.map(item => (
        <Item key={item.id} item={item} />
      ))}
    </ul>
  </div>
);
