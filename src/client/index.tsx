import React from 'react';
import ReactDOM from 'react-dom';
import { App } from '../components/App';

(function() {
  var main = document.getElementById('main');

  ReactDOM.hydrate(<App />, main);
})();
