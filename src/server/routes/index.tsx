import React from 'react';
import ReactDOMServer from 'react-dom/server';

import { App } from '../../components/App';

export default (req: Request, res: Response) => {
  const component = ReactDOMServer .renderToNodeStream(<App />);

  const htmlStart = `
    <html>
      <body>
        <div id="main">`;
        
  const htmlEnd = `</div>
        <script src="/build/main.js"></script>
      </body>
    </html>
  `;

  res.write(htmlStart);

  component.pipe(res, { end: false });

  component.on('end', () => {
    res.write(htmlEnd);
    res.end();
  });
};
