import * as express from 'express';
import { Request, Response } from 'express';

import index from './routes/index';

const app = express();

const {
  PORT = 3000,
} = process.env;

app.use('/build', express.static('build'));

app.get('/', index);

app.listen(PORT, () => {
  console.log('Server started on http://localhost:'+PORT);
});
