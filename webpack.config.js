const path = require('path');
const WebpackShellPlugin = require('webpack-shell-plugin');
const nodeExternals = require('webpack-node-externals');

const {
  NODE_ENV = 'production',
} = process.env;

const BASE = {
  entry: {
    main: './src/index',
    server: './src/server',
  },
  devtool: 'inline-source-map',
  mode: NODE_ENV,
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'build'),
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
      }
    ]
  },
};

const CLIENT = {
  ...BASE,
  watch: NODE_ENV === 'development',
  entry: {
    main: './src/client/index',
  },
};

const SERVER = {
  ...BASE,
  entry: {
    server: './src/server/index',
  },
  plugins: [
    new WebpackShellPlugin({
      onBuildEnd: ['npm run run:dev']
    })
  ],
  target: 'node',
  externals: [nodeExternals()]
}

module.exports = [CLIENT, SERVER];
